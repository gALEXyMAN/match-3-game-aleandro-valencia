﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GridGenerator : EditorWindow
{
    GameObject m_cell;
    int m_rows = 5;
    int m_columns = 5;
    float m_rowSpace = 0.1f;
    float m_colSpace = 0.1f;

    [MenuItem("Window/Grid Generator")]
    public static void ShowWindow()
    {
        GetWindow<GridGenerator>("Grid Generator");
    }

    private void OnGUI()
    {
        EditorGUILayout.LabelField("Grid", EditorStyles.boldLabel);
        m_cell = EditorGUILayout.ObjectField(new GUIContent("Cell", "Gameobject to be used as tile"), m_cell, typeof(GameObject), true) as GameObject;
        m_rows = EditorGUILayout.IntField(new GUIContent("Rows", "Number of rows"), m_rows);
        m_columns = EditorGUILayout.IntField(new GUIContent("Columns", "Number of columns"), m_columns);
        m_rowSpace = EditorGUILayout.FloatField(new GUIContent("Row Spacing", "Space in between each row"), m_rowSpace);
        m_colSpace = EditorGUILayout.FloatField(new GUIContent("Column Spacing", "Space in between each column"), m_colSpace);

        if (GUILayout.Button("Generate Grid"))
        {
            GenerateGrid();
        }
    }

    private void GenerateGrid()
    {
        if (m_cell == null)
        {
            Debug.LogError("Error: Cell gameobject is not set!");
            return;
        }
        if (m_rows < 1 || m_columns < 1)
        {
            Debug.LogError("Error: Rows and Columns must be greater than 0.");
            return;
        }
        if (m_cell.GetComponent<SpriteRenderer>() == null)
        {
            Debug.LogError("Error: Cell SpriteRenderer is missing!");
            return;
        }

        Debug.Log("Generated Grid!");
        GameObject grid = new GameObject();
        grid.name = "Grid";
        grid.AddComponent<GridManager>();
        grid.AddComponent<GridChecker>();
        grid.AddComponent<ShuffleGrid>();

        Vector2 pos = Vector2.zero;
        int cellIndex = 0;

        for (int row = 0; row < m_rows; ++row)
        {
            for (int col = 0; col < m_columns; ++col)
            {
                GameObject cell = Instantiate(m_cell);
                cell.name = "Cell " + cellIndex;
                cellIndex++;

                pos.x = col * cell.GetComponent<SpriteRenderer>().bounds.size.x + (m_colSpace * col);
                pos.y = -(row * cell.GetComponent<SpriteRenderer>().bounds.size.y + (m_rowSpace * row));   // Negative to read from top to bottom
                cell.transform.position = pos;

                cell.transform.SetParent(grid.transform);
                grid.GetComponent<GridManager>().Cells.Add(cell.GetComponent<CellScript>());
            }
        }

        ConnectAdjacentCells(grid);
    }

    /// <summary>
    /// Add adjacent Cells to list of adjacent cells in CellScript
    /// </summary>
    /// <param name="_grid"></param>
    void ConnectAdjacentCells(GameObject _grid)
    {
        // Connect adjacent cells
        CellScript[] cells = _grid.GetComponentsInChildren<CellScript>();
        for (int i = 0; i < cells.Length; ++i)
        {
            bool hasTopNeighbour = i >= m_columns;
            bool hasBottomNeighbour = i < cells.Length - m_columns;
            bool hasLeftNeighbour = i % m_columns == 0 ? false : true;
            bool hasRightNeighbour = (i % m_columns) + 1 == m_columns ? false : true;

            if (hasTopNeighbour)
            {
                cells[i].AdjacentCells[(int)Utils.ADJACENT_TILE.UP] = (cells[i - m_columns]);
                cells[i].AboveCell = cells[i - m_columns];
            }

            if (hasBottomNeighbour)
            {
                cells[i].AdjacentCells[(int)Utils.ADJACENT_TILE.DOWN] = (cells[i + m_columns]);
                cells[i].BelowCell = cells[i + m_columns];
            }

            if (hasLeftNeighbour)
            {
                cells[i].AdjacentCells[(int)Utils.ADJACENT_TILE.LEFT] = (cells[i - 1]);
            }

            if (hasRightNeighbour)
            {
                cells[i].AdjacentCells[(int)Utils.ADJACENT_TILE.RIGHT] = (cells[i + 1]);
            }
        }
    }
}
