﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Objective
{
    public Utils.TILE_TYPE tileType = Utils.TILE_TYPE.CIRCLE;
    public int numTiles = 5;
    [HideInInspector]
    public GameObject uiObject;
}

public class GameManager : MonoBehaviour
{
    [SerializeField] GameObject objectivePanel = null;
    [SerializeField] GameObject objectiveUI = null;
    [SerializeField] Objective[] objectives = null;
    [SerializeField] List<Sprite> tileSprites = null;
    CursorController cursor = null;
    const string keyboardControlsKey = "KeyboardControls";

    GameOver gameOver;
    int completedObjectives = 0;

    void Start()
    {
        gameOver = GetComponent<GameOver>();
        cursor = FindObjectOfType<CursorController>();
        if(PlayerPrefs.GetInt(keyboardControlsKey, 0) == 1)
        {
            cursor.gameObject.SetActive(true);
        }
        else
        {
            cursor.gameObject.SetActive(false);
        }

        if (objectives.Length <= 0)
        {
            Debug.LogError("No objectives set");
            return;
        }

        // Format nicely in panel vertically
        float objectivePanelHeight = objectivePanel.GetComponent<RectTransform>().sizeDelta.y;
        float yPos = objectivePanelHeight / (objectives.Length + 1);

        for (int i = 0; i < objectives.Length; ++i)
        {
            GameObject objectiveInfo = Instantiate(objectiveUI, objectivePanel.transform);
            Vector3 pos = objectiveInfo.GetComponent<RectTransform>().localPosition;
            pos.y = yPos * (i + 1) - (objectivePanelHeight / 2);
            objectiveInfo.GetComponent<RectTransform>().localPosition = pos;

            objectiveInfo.GetComponentInChildren<Text>().text = objectives[i].numTiles.ToString();

            switch (objectives[i].tileType)
            {
                case Utils.TILE_TYPE.CIRCLE:
                    objectiveInfo.name = "ObjectiveCIRCLE";
                    objectiveInfo.GetComponentInChildren<Image>().sprite = tileSprites[(int)Utils.TILE_TYPE.CIRCLE];
                    break;
                case Utils.TILE_TYPE.TRIANGLE:
                    objectiveInfo.name = "ObjectiveTRIANGLE";
                    objectiveInfo.GetComponentInChildren<Image>().sprite = tileSprites[(int)Utils.TILE_TYPE.TRIANGLE];
                    break;
                case Utils.TILE_TYPE.SQUARE:
                    objectiveInfo.name = "ObjectiveSQUARE";
                    objectiveInfo.GetComponentInChildren<Image>().sprite = tileSprites[(int)Utils.TILE_TYPE.SQUARE];
                    break;
                case Utils.TILE_TYPE.DIAMOND:
                    objectiveInfo.name = "ObjectiveDIAMOND";
                    objectiveInfo.GetComponentInChildren<Image>().sprite = tileSprites[(int)Utils.TILE_TYPE.DIAMOND];
                    break;
                case Utils.TILE_TYPE.PENTAGON:
                    objectiveInfo.name = "ObjectivePENTAGON";
                    objectiveInfo.GetComponentInChildren<Image>().sprite = tileSprites[(int)Utils.TILE_TYPE.PENTAGON];
                    break;
                case Utils.TILE_TYPE.HEXAGON:
                    objectiveInfo.name = "ObjectiveHEXAGON";
                    objectiveInfo.GetComponentInChildren<Image>().sprite = tileSprites[(int)Utils.TILE_TYPE.HEXAGON];
                    break;
                default:
                    break;
            }
            objectives[i].uiObject = objectiveInfo;
        }
    }

    public void DecrementCount(Utils.TILE_TYPE _type)
    {
        foreach (Objective objective in objectives)
        {
            if (objective.tileType == _type)
            {
                if (objective.numTiles > 0)
                {
                    objective.numTiles--;
                    objective.uiObject.GetComponentInChildren<Text>().text = objective.numTiles.ToString();
                }
            }
            if (objective.numTiles <= 0)
            {
                if (objective.uiObject.activeSelf)
                {
                    objective.uiObject.SetActive(false);
                    completedObjectives++;
                }
            }
        }

        if (completedObjectives == objectives.Length)
        {
            gameOver.GameOverScreen(true);
        }
    }
}
