﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GameOver : MonoBehaviour
{
    [SerializeField] GameObject gameOverScreen = null;
    [SerializeField] Text winText = null;
    [SerializeField] GameObject mainMenuButton = null;
    [SerializeField] EventSystem uiHandler = null;
    PauseMenuManager pauseMenu;
    AudioManager audioManager;

    void Start()
    {
        pauseMenu = FindObjectOfType<PauseMenuManager>();
        audioManager = FindObjectOfType<AudioManager>();
    }

    public void GameOverScreen(bool _win)
    {
        pauseMenu.Pause();
        if (_win)
        {
            audioManager.Play(AudioManager.SOUND_EFFECT.WIN);
            winText.text = "YOU WIN!!!";
        }
        else
        {
            audioManager.Play(AudioManager.SOUND_EFFECT.LOSE);
            winText.text = "YOU LOSE";
        }
        gameOverScreen.SetActive(true);
        uiHandler.SetSelectedGameObject(mainMenuButton);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
