﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    [SerializeField] Text timerText = null;
    [SerializeField] float startTime = 60.0f;
    [SerializeField] float hintDelay = 5.0f;
    float timeRemaining;
    float hintTimer = 0.0f;
    PauseMenuManager pauseMenu;
    GameOver gameOver;
    Hint hint;

    void Start()
    {
        hint = FindObjectOfType<Hint>();
        pauseMenu = GetComponent<PauseMenuManager>();
        gameOver = GetComponent<GameOver>();
        timeRemaining = startTime;
    }

    void Update()
    {
        if (timerText == null)
        {
            Debug.LogError("TimerText not set!");
            return;
        }

        if (!pauseMenu.Paused)
        {
            if (timeRemaining > 0.0f)
            {
                timeRemaining -= Time.deltaTime;
                timerText.text = "TIME: " + Mathf.RoundToInt(timeRemaining).ToString();
                hintTimer += Time.deltaTime;
                PlayHint();
            }
            else
            {
                // Game Over
                gameOver.GameOverScreen(false);
            }
        }
    }

    public void ResetHintTimer()
    {
        hintTimer = 0.0f;
    }

    void PlayHint()
    {
        if(hintTimer >= hintDelay)
        {
            hint.PlayHint();
            ResetHintTimer();
        }
    }
}
