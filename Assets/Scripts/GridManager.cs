﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    [SerializeField] GameObject shuffleText = null;
    [SerializeField] List<CellScript> cells = new List<CellScript>();
    public List<CellScript> Cells { get { return cells; } }

    AudioManager audioManager;
    GameManager gameManager;
    GridChecker gridChecker;
    ShuffleGrid gridShuffler;
    Animator shuffleTextAnim;
    CursorController cursor;

    // Start is called before the first frame update
    void Start()
    {
        shuffleTextAnim = shuffleText.GetComponent<Animator>();
        audioManager = FindObjectOfType<AudioManager>();
        gameManager = FindObjectOfType<GameManager>();
        cursor = FindObjectOfType<CursorController>();
        gridShuffler = GetComponent<ShuffleGrid>();
        gridChecker = GetComponent<GridChecker>();

        // Clean grid
        for(int i = cells.Count - 1;i >= 0; --i)
        {
            if(cells[i] == null)
            {
               cells.RemoveAt(i);
            }
        }

        GenerateSolvableGrid();
    }

    /// <summary>
    /// Generate grid that's solvable and doesn't contain any match threes
    /// </summary>
    public void GenerateSolvableGrid()
    {
        gridShuffler.GenerateGrid(cells);
    }

    /// <summary>
    /// Returns true if possible match three exists in grid
    /// </summary>
    public bool IsSolvable()
    {
        return gridChecker.CheckIsSolvable(cells);
    }

    /// <summary>
    /// Returns if _cell is in a match three
    /// </summary>
    public bool MatchThreeFound(CellScript _cell, Utils.TILE_TYPE _type)
    {
        return gridChecker.CheckAdjacentMatch(_cell, _type);
    }

    public void CheckCompleteLines()
    {
        // Double check if match three is found
        bool doubleCheck = FindAndSetMatchThree();
        DropCells();

        // Wait until all tiles finish moving
        StartCoroutine(ReCheckCompleteLines(doubleCheck));
    }

    /// <summary>
    /// Reset grid if currently not solvable
    /// </summary>
    public void ResetGridCheck()
    {
        if (!IsSolvable())
        {
            //Reset Grid
            shuffleText.SetActive(true);
            GenerateSolvableGrid();
            CheckCompleteLines();
        }
    }

    /// <summary>
    /// Check adjacent cells recursively setting cell as MATCHED if in a chain of 3 or more
    /// </summary>
    bool CheckAdjacentCellMatched(CellScript _currentCell, Utils.ADJACENT_TILE _dir, bool _setMatched, ref int _count)
    {
        _count++;
        if (_currentCell.AdjacentCells[(int)_dir] == null)
        {
            return SetMatched(_currentCell, _dir, _setMatched, ref _count);
        }

        if (_currentCell.TileType == _currentCell.AdjacentCells[(int)_dir].TileType)
        {
            CheckAdjacentCellMatched(_currentCell.AdjacentCells[(int)_dir], _dir, _setMatched, ref _count);
        }

        return SetMatched(_currentCell, _dir, _setMatched, ref _count);
    }

    /// <summary>
    /// Go through grid setting MATCHED cells if found
    /// </summary>
    /// <returns></returns>
    bool FindAndSetMatchThree()
    {
        bool horizontalMatch = false;
        bool verticalMatch = false;
        bool doubleCheck = false;

        // Find match 3's
        for (int i = 0; i < cells.Count; ++i)
        {
            int count = 0;
            horizontalMatch = CheckAdjacentCellMatched(cells[i], Utils.ADJACENT_TILE.RIGHT, true, ref count);
            count = 0;
            verticalMatch = CheckAdjacentCellMatched(cells[i], Utils.ADJACENT_TILE.DOWN, true, ref count);
            if (horizontalMatch || verticalMatch)
                doubleCheck = true;
        }
        return doubleCheck;
    }

    /// <summary>
    /// If matched, drop cell above until matched cell reaches top of board
    /// </summary>
    void DropCells()
    {
        foreach (CellScript cell in cells)
        {
            if (cell.MatchedHorizontally || cell.MatchedVertically)
            {
                PlayParticles(cell);
                if (cell.AboveCell != null)
                {
                    if (!cell.AboveCell.MatchedHorizontally && !cell.AboveCell.MatchedVertically)
                    {
                        cell.Drop();
                    }
                }
                else
                {
                    cell.ResetTileType();
                }
            }
        }
    }

    /// <summary>
    /// Play particles and hide sprite
    /// </summary>
    void PlayParticles(CellScript _cell)
    {
        if (_cell.GetComponent<SpriteRenderer>().sprite != null)
        {
            _cell.GetComponent<SpriteRenderer>().sprite = null;
            _cell.PlayParticles();
            audioManager.PlayPitchVariance(AudioManager.SOUND_EFFECT.MATCHED, Random.Range(0.0f, 0.05f));
            gameManager.DecrementCount(_cell.TileType);
        }
    }

    /// <summary>
    /// Wait until cells have stopped moving before dropping the remaining cells or check to reset grid
    /// </summary>
    IEnumerator ReCheckCompleteLines(bool _doubleCheck)
    {
        bool tilesMoving = true;
        while (tilesMoving)
        {
            foreach (CellScript cell in cells)
            {
                if (cell.GetComponent<Movement>().Moving)
                {
                    tilesMoving = true;
                    break;
                }
                else
                {
                    tilesMoving = false;
                }
            }
            yield return null;
        }

        if (_doubleCheck)
        {
            DropRemainingCells();
        }
        else
        {
            ResetGridCheck();
            if (cursor != null)
            {
                StartCoroutine(cursor.SetCurrentCellAfterSwipe());
            }
        }
    }

    /// <summary>
    /// Drop if Matched cells haven't reached the top of the board yet
    /// </summary>
    void DropRemainingCells()
    {
        // Drop matched cells that haven't been dropped yet
        DropCells();

        bool stillDropping = false;
        foreach (CellScript cell in cells)
        {
            if (cell.MatchedHorizontally || cell.MatchedVertically)
            {
                stillDropping = true;
                break;
            }
        }

        if (stillDropping)
        {
            // Matched tiles are still dropping
            StartCoroutine(ReCheckCompleteLines(stillDropping));
        }
        else
        {
            // REcheck board when matched tiles have been reset
            CheckCompleteLines();
        }
    }

    /// <summary>
    /// Tag the currentCell as "MATCHED" if row/column of 3 has same tile_type
    /// </summary>
    bool SetMatched(CellScript _currentCell, Utils.ADJACENT_TILE _dir, bool _setMatched, ref int _count)
    {
        if (_count >= 3)
        {
            if (_setMatched)
            {
                if (_dir == Utils.ADJACENT_TILE.RIGHT)
                {
                    _currentCell.SetMatchedHorizontally();
                }
                else
                {
                    _currentCell.SetMatchedVertically();
                }
            }
            return true;
        }
        else
        {
            return false;
        }
    }
}
