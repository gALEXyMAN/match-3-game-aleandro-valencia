﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShuffleGrid : MonoBehaviour
{
    GridManager grid;

    void Start()
    {
        grid = GetComponent<GridManager>();
    }

    /// <summary>
    /// Generates grid with no match threes
    /// </summary>
    public void GenerateGrid(List<CellScript> _cells)
    {
        while (MatchThreeFound() || !grid.IsSolvable())
        {
            RandomiseCells(_cells);
        }
    }

    bool MatchThreeFound()
    {
        foreach (CellScript cell in grid.Cells)
        {
            if (grid.MatchThreeFound(cell, cell.TileType))
            {
                return true;
            }
        }
        return false;
    }

    void RandomiseCells(List<CellScript> _cells)
    {
        foreach (CellScript cell in _cells)
        {
            cell.ResetTileType();
        }
    }
}
