﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    Animator[] animators;

    // Start is called before the first frame update
    void Start()
    {
        animators = GetComponentsInChildren<Animator>();
    }

    /// <summary>
    /// Play animations in all children (face and mouth)
    /// </summary>
    public void PlayAnimation()
    {
        foreach(Animator anim in animators)
        {
            anim.SetTrigger("PlayFaceAnim");
        }
    }
}
