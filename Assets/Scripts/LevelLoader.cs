﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public void LoadLevel(string _level)
    {
        SceneManager.LoadScene(_level);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
