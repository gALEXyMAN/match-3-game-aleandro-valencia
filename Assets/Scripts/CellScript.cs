﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellScript : MonoBehaviour
{
    [SerializeField] Utils.TILE_TYPE tileType = Utils.TILE_TYPE.CIRCLE;
    [SerializeField] CellScript[] adjacentCells = new CellScript[4];// READONLY, for testing/debugging
    [SerializeField] CellScript aboveCell;
    [SerializeField] CellScript belowCell;
    [SerializeField] bool matchedHorizontally = false;
    [SerializeField] bool matchedVertically = false;
    [SerializeField] List<Sprite> tileSprites = null;
    new SpriteRenderer renderer;
    ParticleSystem particles;
    Vector3 startPos;

    public Utils.TILE_TYPE TileType { get { return tileType; } set { tileType = value; } }
    public CellScript[] AdjacentCells { get { return adjacentCells; } }
    public CellScript AboveCell { get { return aboveCell; } set { aboveCell = value; } }
    public CellScript BelowCell { get { return belowCell; } set { belowCell = value; } }
    public bool MatchedHorizontally { get { return matchedHorizontally; } }
    public bool MatchedVertically { get { return matchedVertically; } }
    public Vector3 StartPos { get { return startPos; } set { startPos = value; } }

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        renderer = GetComponent<SpriteRenderer>();
        particles = GetComponent<ParticleSystem>();
        ResetTileType();  // Randomise cells at start
        //SetSprite();
    }

    /// <summary>
    /// Check if cell exists in _direction then swap tile types and set appropriate Sprite
    /// </summary>
    public void SwapCells(Utils.ADJACENT_TILE _direction)
    {
        CellScript adjacentCell = adjacentCells[(int)_direction];
        if (adjacentCell == null)
        {
            return;
        }
        StartCoroutine(GetComponent<Movement>().SwapTiles(adjacentCells[(int)_direction].transform, _direction, false));
    }

    /// <summary>
    /// Randomise tileType and set sprite accordingly 
    /// </summary>
    public void ResetTileType()
    {
        tileType = (Utils.TILE_TYPE)Random.Range(0, tileSprites.Count);
        SetSprite();
        ResetMatchedState();
    }

    /// <summary>
    /// Set Sprite according to tileType
    /// </summary>
    public void SetSprite()
    {
        switch (tileType)
        {
            case Utils.TILE_TYPE.CIRCLE:
                renderer.sprite = tileSprites[(int)Utils.TILE_TYPE.CIRCLE];
                break;
            case Utils.TILE_TYPE.TRIANGLE:
                renderer.sprite = tileSprites[(int)Utils.TILE_TYPE.TRIANGLE];
                break;
            case Utils.TILE_TYPE.SQUARE:
                renderer.sprite = tileSprites[(int)Utils.TILE_TYPE.SQUARE];
                break;
            case Utils.TILE_TYPE.DIAMOND:
                renderer.sprite = tileSprites[(int)Utils.TILE_TYPE.DIAMOND];
                break;
            case Utils.TILE_TYPE.PENTAGON:
                renderer.sprite = tileSprites[(int)Utils.TILE_TYPE.PENTAGON];
                break;
            case Utils.TILE_TYPE.HEXAGON:
                renderer.sprite = tileSprites[(int)Utils.TILE_TYPE.HEXAGON];
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// If cell is not at the top of grid, Swap tile with above tile, else set tile to random type
    /// </summary>
    public void Drop()
    {
        if (aboveCell != null)
        {
            StartCoroutine(GetComponent<Movement>().SwapTiles(aboveCell.transform, Utils.ADJACENT_TILE.UP, true));
        }
        else
        {
            ResetTileType();
        }
    }

    /// <summary>
    /// Set matchedHorizontally to true
    /// </summary>
    public void SetMatchedHorizontally() { matchedHorizontally = true; }

    /// <summary>
    /// Set matchedVertically to true
    /// </summary>
    public void SetMatchedVertically() { matchedVertically = true; }

    /// <summary>
    /// Reset Matched state to false
    /// </summary>
    public void ResetMatchedState()
    {
        matchedHorizontally = false;
        matchedVertically = false;
    }

    public void PlayParticles()
    {
        if (particles != null)
        {
            particles.Play();
        }
    }

    /// <summary>
    /// Swap name and adjacent cells with the target cell that is in adjacentDirection to current cell
    /// </summary>
    public void SwapProperties(Utils.ADJACENT_TILE _adjacentDir)
    {
        // Check if target exists
        CellScript target = adjacentCells[(int)_adjacentDir];
        if (target == null)
        {
            return;
        }

        //Swap properties with target
        string temp = name;
        name = target.name;
        target.name = temp;
        for (int i = 0; i < 4; ++i)
        {
            // Set neighbour's neighbour to the swapped cell
            Utils.ADJACENT_TILE oppositeDir = Utils.OppositeDirection((Utils.ADJACENT_TILE)i);
            SwapAdjacentCells(i, oppositeDir, target);
            Swap<CellScript>(ref adjacentCells[i], ref target.adjacentCells[i]);
        }

        // Prevent cell from setting self as adjacent
        adjacentCells[(int)Utils.OppositeDirection(_adjacentDir)] = target;
        target.adjacentCells[(int)_adjacentDir] = this;

        aboveCell = adjacentCells[(int)Utils.ADJACENT_TILE.UP];
        belowCell = adjacentCells[(int)Utils.ADJACENT_TILE.DOWN];
        target.aboveCell = target.adjacentCells[(int)Utils.ADJACENT_TILE.UP];
        target.belowCell = target.adjacentCells[(int)Utils.ADJACENT_TILE.DOWN];

        // Reset the aboveCell of the cell below swapped cell
        if (target.belowCell != null)
        {
            target.belowCell.aboveCell = target;
        }
        if (belowCell != null)
        {
            belowCell.aboveCell = this;
        }

        // Swap start Positions
        Swap<Vector3>(ref startPos, ref target.startPos);
    }

    /// <summary>
    /// Reset the adjacent cells of the neighbours of the currentCell and targetCell
    /// </summary>
    void SwapAdjacentCells(int _adjacent, Utils.ADJACENT_TILE _dir, CellScript _swapTarget)
    {
        CellScript neighbour = adjacentCells[_adjacent];
        CellScript targetsNeighbour = _swapTarget.adjacentCells[_adjacent];

        if (neighbour != null)
        {
            adjacentCells[_adjacent].adjacentCells[(int)_dir] = _swapTarget;
        }
        if (targetsNeighbour != null)
        {
            _swapTarget.adjacentCells[_adjacent].adjacentCells[(int)_dir] = this;
        }
    }

    void Swap<T>(ref T lhs, ref T rhs)
    {
        T temp = lhs;
        lhs = rhs;
        rhs = temp;
    }
}
