﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    PauseMenuManager pauseMenu;
    GridManager gridManager;
    CellScript cell;
    new SpriteRenderer renderer;
    CursorController cursor;
    [SerializeField] float dragLength = 0.5f;
    float width;
    float height;
    bool selected = false;
    Vector3 touchStartPos = Vector3.zero;
    Vector3 touchEndPos = Vector3.zero;
    Vector3 touchPos = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        pauseMenu = FindObjectOfType<PauseMenuManager>();
        gridManager = FindObjectOfType<GridManager>();
        renderer = GetComponent<SpriteRenderer>();
        cell = GetComponent<CellScript>();
        cursor = FindObjectOfType<CursorController>();
        width = renderer.bounds.size.x;
        height = renderer.bounds.size.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (pauseMenu.Paused || Utils.movingTile)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            SelectCell(touchPos);
        }
        else if (Input.GetMouseButton(0))
        {
            if (selected)
            {
                touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Swipe(touchPos);
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            selected = false;
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            FindObjectOfType<Hint>().PlayHint();
        }
    }

    /// <summary>
    /// Detect if the player touched this cell
    /// </summary>
    void SelectCell(Vector3 _touchPos)
    {
        bool inXBounds = _touchPos.x < transform.position.x + width / 2 && _touchPos.x > transform.position.x - width / 2;
        bool inYBounds = _touchPos.y < transform.position.y + height / 2 && _touchPos.y > transform.position.y - height / 2;
        if (inXBounds && inYBounds)
        {
            touchStartPos = _touchPos;
            selected = true;
            if (cursor != null)
            {
                cursor.SetCurrentCell(cell);
            }
        }
    }

    /// <summary>
    /// Swap cells with the selected cell and the cell in the direction the player swiped
    /// </summary>
    void Swipe(Vector3 _touchPos)
    {
        touchEndPos = _touchPos;
        Vector3 distance = touchEndPos - touchStartPos;
        if (Mathf.Abs(distance.x) > dragLength || Mathf.Abs(distance.y) > dragLength)
        {
            if (Mathf.Abs(distance.x) > Mathf.Abs(distance.y))
            {
                if (distance.x > 0)
                {
                    if (cursor != null)
                    {
                        cursor.SetCurrentCell(cell.AdjacentCells[(int)Utils.ADJACENT_TILE.RIGHT]);
                    }
                    cell.SwapCells(Utils.ADJACENT_TILE.RIGHT);
                    selected = false;
                }
                else
                {
                    if (cursor != null)
                    {
                        cursor.SetCurrentCell(cell.AdjacentCells[(int)Utils.ADJACENT_TILE.LEFT]);
                    }
                    cell.SwapCells(Utils.ADJACENT_TILE.LEFT);
                    selected = false;
                }
            }
            else
            {
                if (distance.y > 0)
                {
                    if (cursor != null)
                    {
                        cursor.SetCurrentCell(cell.AdjacentCells[(int)Utils.ADJACENT_TILE.UP]);
                    }
                    cell.SwapCells(Utils.ADJACENT_TILE.UP);
                    selected = false;
                }
                else
                {
                    if (cursor != null)
                    {
                        cursor.SetCurrentCell(cell.AdjacentCells[(int)Utils.ADJACENT_TILE.DOWN]);
                    }
                    cell.SwapCells(Utils.ADJACENT_TILE.DOWN);
                    selected = false;
                }
            }
        }
    }
}
