﻿using UnityEngine.Audio;
using UnityEngine;
using System;

public class AudioManager : MonoBehaviour
{
    public enum SOUND_EFFECT
    {
        SWAP = 0,
        MATCHED,
        PAUSED,
        LOSE,
        WIN
    }

    public static AudioManager instance;
    public AudioSource music;
    public AudioSource[] sfx;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        music.volume = PlayerPrefs.GetFloat(Utils.musicVolume, 1.0f);
        foreach(AudioSource sound in sfx)
        {
            sound.volume = PlayerPrefs.GetFloat(Utils.sfxVolume, 1.0f);
        }
    }

    public void Play(SOUND_EFFECT _sound)
    {
        sfx[(int)_sound].Play();
    }

    /// <summary>
    /// Play sound with slight offset to reduce repetitiveness
    /// </summary>
    public void PlayPitchVariance(SOUND_EFFECT _sound, float _pitchOffset)
    {
        sfx[(int)_sound].pitch = 1.0f + _pitchOffset;
        sfx[(int)_sound].Play();
    }

    public void SetMusicVolume(float _value)
    {
        music.volume = _value;
        PlayerPrefs.SetFloat(Utils.musicVolume, _value);
    }

    public void SetSFXVolume(float _value)
    {
        foreach(AudioSource sound in sfx)
        {
            sound.volume = _value;
        }
        PlayerPrefs.SetFloat(Utils.sfxVolume, _value);
    }
}
