﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] EventSystem uiHandler = null;
    [SerializeField] GameObject optionsMenu = null;
    [SerializeField] GameObject optionsButton = null;
    [SerializeField] GameObject optionsMenuSelected = null;

    public void OpenOptionsMenu()
    {
        optionsMenu.SetActive(true);
        uiHandler.SetSelectedGameObject(optionsMenuSelected);
    }

    public void CloseOptionsMenu()
    {
        optionsMenu.SetActive(false);
        uiHandler.SetSelectedGameObject(optionsButton);
    }
}
