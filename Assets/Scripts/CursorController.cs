﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorController : MonoBehaviour
{
    [SerializeField] CellScript currentCell = null;
    [SerializeField] Color selectedColour = Color.green;
    [SerializeField] Color normalColour = Color.black;
    GridManager grid = null;
    PauseMenuManager pauseMenu = null;
    SpriteRenderer sprite = null;
    string currentCellName = "Cell 0";
    bool selected = false;
    bool moved = false;

    void Start()
    {
        pauseMenu = FindObjectOfType<PauseMenuManager>();
        sprite = GetComponent<SpriteRenderer>();
        grid = FindObjectOfType<GridManager>();
        currentCellName = currentCell.name;
    }

    public void SetCurrentCell(CellScript _cell)
    {
        currentCell = _cell;
        currentCellName = currentCell.name;
        transform.position = currentCell.transform.position;
    }

    void GetCellName()
    {
        currentCellName = currentCell.name;
    }

    CellScript WrapAround(CellScript _adjacentCell, Utils.ADJACENT_TILE _dir)
    {
        if (_adjacentCell.AdjacentCells[(int)_dir] != null)
        {
            return WrapAround(_adjacentCell.AdjacentCells[(int)_dir], _dir);
        }
        return _adjacentCell;
    }

    public IEnumerator SetCurrentCellAfterSwipe()
    {
        while (Utils.movingTile)
        {
            yield return null;
        }
        currentCell = GameObject.Find(currentCellName).GetComponent<CellScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if (pauseMenu.Paused || Utils.movingTile)
        {
            return;
        }

        if (Input.GetButtonDown("Submit"))
        {
            selected = !selected;
        }

        if (selected)
        {
            sprite.color = selectedColour;

            if (Input.GetAxisRaw("Horizontal") > 0)
            {
                selected = false;
                currentCell.SwapCells(Utils.ADJACENT_TILE.RIGHT);
            }
            else if (Input.GetAxisRaw("Horizontal") < 0)
            {
                selected = false;
                currentCell.SwapCells(Utils.ADJACENT_TILE.LEFT);
            }
            else if (Input.GetAxisRaw("Vertical") > 0)
            {
                selected = false;
                currentCell.SwapCells(Utils.ADJACENT_TILE.UP);
            }
            else if (Input.GetAxisRaw("Vertical") < 0)
            {
                selected = false;
                currentCell.SwapCells(Utils.ADJACENT_TILE.DOWN);
            }
        }
        else
        {
            sprite.color = normalColour;
        }

        if (!moved)
        {
            if (Input.GetAxisRaw("Horizontal") > 0)
            {
                MoveCursor(Utils.ADJACENT_TILE.RIGHT);
            }
            else if (Input.GetAxisRaw("Horizontal") < 0)
            {
                MoveCursor(Utils.ADJACENT_TILE.LEFT);
            }

            if (Input.GetAxisRaw("Vertical") > 0)
            {
                MoveCursor(Utils.ADJACENT_TILE.UP);
            }
            else if (Input.GetAxisRaw("Vertical") < 0)
            {
                MoveCursor(Utils.ADJACENT_TILE.DOWN);
            }
        }
        else
        {
            if (Input.GetAxisRaw("Horizontal") == 0 &&
                Input.GetAxisRaw("Vertical") == 0)
            {
                moved = false;
            }
            Vector3 pos = transform.position;
            pos.z = -0.1f;
            transform.position = pos;
        }
    }

    void MoveCursor(Utils.ADJACENT_TILE _dir)
    {
        if (currentCell.AdjacentCells[(int)_dir] != null)
        {
            currentCell = currentCell.AdjacentCells[(int)_dir];
        }
        else
        {
            Utils.ADJACENT_TILE oppositeDir = Utils.OppositeDirection(_dir);
            currentCell = WrapAround(currentCell.AdjacentCells[(int)oppositeDir], oppositeDir);
        }
        transform.position = currentCell.transform.position;
        moved = true;
        GetCellName();
    }
}
