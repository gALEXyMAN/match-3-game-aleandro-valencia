﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridChecker : MonoBehaviour
{
    /// <summary>
    /// Checks adjacent cells to see if swap generates a match 3
    /// Returns true as soon as possible match 3 is found
    /// </summary>
    public bool CheckIsSolvable(List<CellScript> _cells)
    {
        foreach (CellScript cell in _cells)
        {
            for (int i = 0; i < (int)Utils.ADJACENT_TILE.MAX_DIRECTION; ++i)
            {
                CellScript adjacentCell = cell.AdjacentCells[i];
                if (adjacentCell == null)
                    continue;

                if (i == (int)Utils.ADJACENT_TILE.UP || i == (int)Utils.ADJACENT_TILE.DOWN)
                {
                    Utils.ADJACENT_TILE leftTile = Utils.ADJACENT_TILE.LEFT;
                    Utils.ADJACENT_TILE rightTile = Utils.ADJACENT_TILE.RIGHT;
                    if (FindPossibleMatchThree(adjacentCell, leftTile, cell.TileType) ||
                        PossibleMatchThreeSameAxis(adjacentCell, (Utils.ADJACENT_TILE)i, cell.TileType))
                    {
                        return true;
                    }
                    if (FindPossibleMatchThree(adjacentCell, rightTile, cell.TileType) ||
                        PossibleMatchThreeSameAxis(adjacentCell, (Utils.ADJACENT_TILE)i, cell.TileType))
                    {
                        return true;
                    }
                }
                else
                {
                    Utils.ADJACENT_TILE upTile = Utils.ADJACENT_TILE.UP;
                    Utils.ADJACENT_TILE downTile = Utils.ADJACENT_TILE.DOWN;
                    if (FindPossibleMatchThree(adjacentCell, upTile, cell.TileType) ||
                        PossibleMatchThreeSameAxis(adjacentCell, (Utils.ADJACENT_TILE)i, cell.TileType))
                    {
                        return true;
                    }
                    if (FindPossibleMatchThree(adjacentCell, downTile, cell.TileType) ||
                        PossibleMatchThreeSameAxis(adjacentCell, (Utils.ADJACENT_TILE)i, cell.TileType))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /// <summary>
    /// Returns if _cell is in a match three
    /// </summary>
    public bool CheckAdjacentMatch(CellScript _cell, Utils.TILE_TYPE _type)
    {
        Utils.ADJACENT_TILE up = Utils.ADJACENT_TILE.UP;
        Utils.ADJACENT_TILE down = Utils.ADJACENT_TILE.DOWN;
        Utils.ADJACENT_TILE left = Utils.ADJACENT_TILE.LEFT;
        Utils.ADJACENT_TILE right = Utils.ADJACENT_TILE.RIGHT;

        if (CheckNeighbourMatchThree(_cell, _type, up)) return true;
        if (CheckNeighbourMatchThree(_cell, _type, down)) return true;
        if (CheckNeighbourMatchThree(_cell, _type, left)) return true;
        if (CheckNeighbourMatchThree(_cell, _type, right)) return true;

        // middle vertical
        if (_cell.AdjacentCells[(int)up] != null &&
            _cell.AdjacentCells[(int)down] != null)
        {
            if (_cell.AdjacentCells[(int)up].TileType == _type &&
                _cell.AdjacentCells[(int)down].TileType == _type)
            {
                return true;
            }
        }

        // middle horizontal
        if (_cell.AdjacentCells[(int)left] != null &&
            _cell.AdjacentCells[(int)right] != null)
        {
            if (_cell.AdjacentCells[(int)left].TileType == _type &&
                _cell.AdjacentCells[(int)right].TileType == _type)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Check match three if swiped on same axis eg.
    /// XOXX returns true
    /// </summary>
    bool PossibleMatchThreeSameAxis(CellScript _adjacentCell, Utils.ADJACENT_TILE _dir, Utils.TILE_TYPE _matchType)
    {
        if (_adjacentCell.AdjacentCells[(int)_dir] == null ||
           _adjacentCell.AdjacentCells[(int)_dir].AdjacentCells[(int)_dir] == null)
        {
            return false;
        }

        if (_adjacentCell.TileType == _matchType &&
           _adjacentCell.AdjacentCells[(int)_dir].AdjacentCells[(int)_dir].TileType == _matchType)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Checks the cell next to adjacent cell to see if tile_type matches and match 3 is found
    /// </summary>
    bool FindPossibleMatchThree(CellScript _adjacentCell, Utils.ADJACENT_TILE _dir, Utils.TILE_TYPE _matchType)
    {
        if (_adjacentCell.AdjacentCells[(int)_dir] == null)
        {
            return false;
        }

        if (_adjacentCell.AdjacentCells[(int)_dir].TileType == _matchType)
        {
            // line in opposite axis of swipe
            // eg   XXO
            //      OOX
            if (_adjacentCell.AdjacentCells[(int)_dir].AdjacentCells[(int)_dir] != null)
            {
                if (_adjacentCell.AdjacentCells[(int)_dir].AdjacentCells[(int)_dir].TileType == _matchType)
                {
                    return true;
                }
            }
            // v shape
            // eg   XOX 
            //      OXO
            Utils.ADJACENT_TILE oppositeDir = Utils.OppositeDirection(_dir);
            if (_adjacentCell.AdjacentCells[(int)oppositeDir] != null)
            {
                if (_adjacentCell.AdjacentCells[(int)oppositeDir].TileType == _matchType)
                {
                    return true;
                }
            }
        }

        return false;
    }

    /// <summary>
    /// Check if _cell is on edge of match three in _dir
    /// eg. oo0
    /// </summary>
    bool CheckNeighbourMatchThree(CellScript _cell, Utils.TILE_TYPE _type, Utils.ADJACENT_TILE _dir)
    {
        if (_cell.AdjacentCells[(int)_dir] != null &&
           _cell.AdjacentCells[(int)_dir].AdjacentCells[(int)_dir] != null)
        {
            if (_cell.AdjacentCells[(int)_dir].TileType == _type &&
                _cell.AdjacentCells[(int)_dir].AdjacentCells[(int)_dir].TileType == _type)
            {
                return true;
            }
        }
        return false;
    }

}
