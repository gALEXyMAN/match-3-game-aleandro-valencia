﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class PauseMenuManager : MonoBehaviour
{
    [SerializeField] EventSystem uiHandler = null;
    [SerializeField] GameObject pauseMenu = null;
    [SerializeField] GameObject resumeButton = null;
    AudioManager audioManager;
    private bool paused = false;
    public bool Paused {get {return paused;}}

    void Start()
    {
        audioManager = FindObjectOfType<AudioManager>();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePause();
        }
    }

    public void Pause()
    {
        paused = true;
    }

    public void TogglePause()
    {
        paused = !paused;
        if(paused) 
        {
            PauseGame();
        }
        else
        {
            ResumeGame();
        }
    }

    public void PauseGame()
    {
        paused = true;
        pauseMenu.SetActive(true);
        audioManager.PlayPitchVariance(AudioManager.SOUND_EFFECT.PAUSED, Random.Range(0.0f, 0.1f));
        uiHandler.SetSelectedGameObject(resumeButton);
    }

    public void ResumeGame()
    {
        paused = false;
        pauseMenu.SetActive(false);
        audioManager.PlayPitchVariance(AudioManager.SOUND_EFFECT.PAUSED, Random.Range(0.0f, 0.1f));
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }
}
