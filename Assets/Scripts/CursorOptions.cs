﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CursorOptions : MonoBehaviour
{
    const string keyboardControlsKey = "KeyboardControls";
    Toggle toggle = null;

    void Start()
    {
        toggle = GetComponent<Toggle>();
        toggle.isOn = PlayerPrefs.GetInt(keyboardControlsKey, 0) == 1 ? true : false;
    }

    public void ToggleKeyboardControls()
    {
        bool toggleValue = toggle.isOn;
        int keyboardControls = toggleValue == true ? 1 : 0;
        PlayerPrefs.SetInt(keyboardControlsKey, keyboardControls);
    }
}
