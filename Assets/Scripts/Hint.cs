﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hint : MonoBehaviour
{
    GridManager grid;

    // Start is called before the first frame update
    void Start()
    {
        grid = FindObjectOfType<GridManager>();
    }

    /// <summary>
    /// Randomly shuffle the elements in List
    /// </summary>
    List<int> ShuffleList(List<int> _list)
    {
        List<int> temp = new List<int>();
        int index = 0;
        while (_list.Count > 0)
        {
            index = Random.Range(0, _list.Count);
            temp.Add(_list[index]);
            _list.Remove(_list[index]);
        }

        return temp;
    }

    /// <summary>
    /// Return list with random order of cells
    /// </summary>
    List<int> RandomiseCellOrder()
    {
        List<int> order = new List<int>();
        for (int i = 0; i < grid.Cells.Count; ++i)
        {
            order.Add(i);
        }
        order = ShuffleList(order);
        return order;
    }

    /// <summary>
    /// Find a potential match three and play hint animation on selected cells
    /// </summary>
    public bool PlayHint()
    {
        List<int> randomOrder = RandomiseCellOrder();

        for (int j = 0; j < randomOrder.Count; ++j)
        {
            // Random cell
            CellScript cell = grid.Cells[randomOrder[j]];
            
            for (int i = 0; i < (int)Utils.ADJACENT_TILE.MAX_DIRECTION; ++i)
            {
                CellScript adjacentCell = cell.AdjacentCells[i];
                if (adjacentCell == null)
                    continue;

                if (i == (int)Utils.ADJACENT_TILE.UP || i == (int)Utils.ADJACENT_TILE.DOWN)
                {
                    Utils.ADJACENT_TILE leftTile = Utils.ADJACENT_TILE.LEFT;
                    Utils.ADJACENT_TILE rightTile = Utils.ADJACENT_TILE.RIGHT;

                    if (CheckHint(cell, adjacentCell, leftTile, rightTile, (Utils.ADJACENT_TILE)i))
                    {
                        return true;
                    }
                }
                else
                {
                    Utils.ADJACENT_TILE upTile = Utils.ADJACENT_TILE.UP;
                    Utils.ADJACENT_TILE downTile = Utils.ADJACENT_TILE.DOWN;

                    if (CheckHint(cell, adjacentCell, upTile, downTile, (Utils.ADJACENT_TILE)i))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /// <summary>
    /// Check match three on passed in axis and play animation if hint exists
    /// </summary>
    bool CheckHint(CellScript _cell, CellScript _adjacentCell, Utils.ADJACENT_TILE _negativeAxis, Utils.ADJACENT_TILE _positiveAxis, Utils.ADJACENT_TILE _adjacentDir)
    {
        if (FoundHint(_adjacentCell, _negativeAxis, _cell.TileType) ||
            FoundHint(_adjacentCell, _positiveAxis, _cell.TileType) ||
            HintSameAxis(_adjacentCell, _adjacentDir, _cell.TileType))
        {
            _cell.GetComponent<AnimationManager>().PlayAnimation();
            return true;
        }
        return false;
    }

    /// <summary>
    /// Checks the cell next to adjacent cell to see if tile_type matches and match 3 is found
    /// </summary>
    public bool FoundHint(CellScript _adjacentCell, Utils.ADJACENT_TILE _dir, Utils.TILE_TYPE _matchType)
    {
        if (_adjacentCell.AdjacentCells[(int)_dir] == null)
        {
            return false;
        }

        if (_adjacentCell.AdjacentCells[(int)_dir].TileType == _matchType)
        {
            if (HintSideCell(_adjacentCell, _dir, _matchType) ||
                HintMiddleCell(_adjacentCell, _dir, _matchType))
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Check match three if swiped on side of consecutive tiles eg.
    /// XOO
    /// OXX
    /// </summary>
    bool HintSideCell(CellScript _adjacentCell, Utils.ADJACENT_TILE _dir, Utils.TILE_TYPE _matchType)
    {
        if (_adjacentCell.AdjacentCells[(int)_dir].AdjacentCells[(int)_dir] != null)
        {
            if (_adjacentCell.AdjacentCells[(int)_dir].AdjacentCells[(int)_dir].TileType == _matchType)
            {
                _adjacentCell.AdjacentCells[(int)_dir].GetComponent<AnimationManager>().PlayAnimation();
                _adjacentCell.AdjacentCells[(int)_dir].AdjacentCells[(int)_dir].GetComponent<AnimationManager>().PlayAnimation();
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Check match three if swiped in between tiles eg
    /// XOX
    /// OXO
    /// </summary>
    bool HintMiddleCell(CellScript _adjacentCell, Utils.ADJACENT_TILE _dir, Utils.TILE_TYPE _matchType)
    {
        Utils.ADJACENT_TILE oppositeDir = Utils.OppositeDirection(_dir);
        if (_adjacentCell.AdjacentCells[(int)oppositeDir] != null)
        {
            if (_adjacentCell.AdjacentCells[(int)oppositeDir].TileType == _matchType)
            {
                _adjacentCell.AdjacentCells[(int)_dir].GetComponent<AnimationManager>().PlayAnimation();
                _adjacentCell.AdjacentCells[(int)oppositeDir].GetComponent<AnimationManager>().PlayAnimation();
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Check match three if swiped on same axis eg.
    /// XOXX 
    /// </summary>
    public bool HintSameAxis(CellScript _adjacentCell, Utils.ADJACENT_TILE _dir, Utils.TILE_TYPE _matchType)
    {
        if (_adjacentCell.AdjacentCells[(int)_dir] == null ||
           _adjacentCell.AdjacentCells[(int)_dir].AdjacentCells[(int)_dir] == null)
        {
            return false;
        }

        if (_adjacentCell.TileType == _matchType &&
           _adjacentCell.AdjacentCells[(int)_dir].AdjacentCells[(int)_dir].TileType == _matchType)
        {
            _adjacentCell.GetComponent<AnimationManager>().PlayAnimation();
            _adjacentCell.AdjacentCells[(int)_dir].AdjacentCells[(int)_dir].GetComponent<AnimationManager>().PlayAnimation();
            return true;
        }
        return false;
    }
}
