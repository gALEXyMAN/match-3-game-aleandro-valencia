﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeSlider : MonoBehaviour
{
    enum VOLUME_SLIDER
    {
        MUSIC,
        SFX
    }
    [SerializeField] VOLUME_SLIDER type = VOLUME_SLIDER.MUSIC;
    AudioManager audioManager;
    Slider slider;

    void Awake()
    {
        audioManager = FindObjectOfType<AudioManager>();
        slider = GetComponent<Slider>();
        if(type == VOLUME_SLIDER.MUSIC)
        {
            slider.value = PlayerPrefs.GetFloat(Utils.musicVolume, 1.0f);
        }
        else if(type == VOLUME_SLIDER.SFX)
        {
            slider.value = PlayerPrefs.GetFloat(Utils.sfxVolume, 1.0f);
        }
    }

    public void SetMusicVolume()
    {
        audioManager.SetMusicVolume(slider.value);
    }

    public void SetSFXVolume()
    {
        audioManager.SetSFXVolume(slider.value);
    }
}
