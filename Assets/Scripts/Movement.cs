﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] AnimationCurve movementCurve = AnimationCurve.Linear(0, 0, 1, 1);
    [SerializeField] float movementSpeed = 3.0f;
    [SerializeField] bool moving = false;
    public bool Moving { get { return moving; } }
    AudioManager audioManager;
    GridManager gridManager;
    CellScript cell;
    Timer gameTimer;

    void Start()
    {
        audioManager = FindObjectOfType<AudioManager>();
        gridManager = FindObjectOfType<GridManager>();
        gameTimer = FindObjectOfType<Timer>();
        cell = GetComponent<CellScript>();
    }

    /// <summary>
    /// Moving animation of swap
    /// </summary>
    public IEnumerator SwapPositions(Transform _target, Vector3 _startPos, Vector3 _targetPos)
    {
        Vector3 currentPos = _startPos;
        Vector3 targetCurrentPos = _targetPos;
        float timer = 0.0f;
        float displacementMultiplier = 0.0f;

        while (currentPos != _targetPos)
        {
            displacementMultiplier = Mathf.Clamp01((movementCurve.Evaluate(timer) * movementSpeed));
            currentPos = _startPos + displacementMultiplier * (_targetPos - _startPos);
            targetCurrentPos = _targetPos + displacementMultiplier * (_startPos - _targetPos);
            timer += Time.deltaTime;
            transform.position = currentPos;
            _target.position = targetCurrentPos;
            yield return null;
        }
        yield return null;
    }

    /// <summary>
    /// Check if swap created a MATCH THREE
    /// </summary>
    bool CheckMatch(Transform _target)
    {
        for (int i = 0; i < (int)Utils.ADJACENT_TILE.MAX_DIRECTION; ++i)
        {
            if (cell.AdjacentCells[i] != null)
            {
                if (gridManager.MatchThreeFound(cell, cell.TileType) ||
                    gridManager.MatchThreeFound(_target.GetComponent<CellScript>(), _target.GetComponent<CellScript>().TileType))
                {
                    return true;
                }
            }
        }
        return false;
    }

    /// <summary>
    /// Animates movement of the tiles that are swapping places
    /// Checks grid for complete lines after swap
    /// </summary>
    public IEnumerator SwapTiles(Transform _target, Utils.ADJACENT_TILE _dir, bool _drop)
    {
        Vector3 startPos = cell.StartPos;
        Vector3 targetPos = _target.GetComponent<CellScript>().StartPos;
        Utils.movingTile = true;
        moving = true;

        gameTimer.ResetHintTimer();
        audioManager.PlayPitchVariance(AudioManager.SOUND_EFFECT.SWAP, Random.Range(-0.1f, 0.1f));
        yield return SwapPositions(_target, startPos, targetPos);
        cell.SwapProperties(_dir);

        bool matched = CheckMatch(_target);

        if (!_drop) // Player swapped these tiles
        {
            if (!matched)
            {
                // Swap back
                audioManager.PlayPitchVariance(AudioManager.SOUND_EFFECT.SWAP, Random.Range(-0.1f, 0.1f));
                yield return SwapPositions(_target, targetPos, startPos);
                cell.SwapProperties(Utils.OppositeDirection(_dir));
            }
            else
            {
                gridManager.CheckCompleteLines();
            }
        }
        moving = false;
        Utils.movingTile = false;
    }
}
