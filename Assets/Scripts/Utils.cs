﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{
    public enum TILE_TYPE
    {
        CIRCLE = 0,
        TRIANGLE,
        SQUARE,
        DIAMOND,
        PENTAGON,
        HEXAGON,
        MAX_TILE_TYPE
    }

    public enum ADJACENT_TILE
    {
        UP = 0,
        DOWN,
        LEFT,
        RIGHT,
        MAX_DIRECTION
    }

    public static bool movingTile = false;
    public const string musicVolume = "MusicVolume";
    public const string sfxVolume = "SFXVolume";

    public static Utils.ADJACENT_TILE OppositeDirection(Utils.ADJACENT_TILE _dir)
    {
        switch (_dir)
        {
            case Utils.ADJACENT_TILE.UP:
                return Utils.ADJACENT_TILE.DOWN;
            case Utils.ADJACENT_TILE.DOWN:
                return Utils.ADJACENT_TILE.UP;
            case Utils.ADJACENT_TILE.LEFT:
                return Utils.ADJACENT_TILE.RIGHT;
            case Utils.ADJACENT_TILE.RIGHT:
                return Utils.ADJACENT_TILE.LEFT;
            default:
                return Utils.ADJACENT_TILE.MAX_DIRECTION;
        }
    }
}
